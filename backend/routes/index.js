var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/info', function(req, res, next) {
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify({ 
    title: 'awesomepage',
    pageLayout : {
      // 'type' : ['menu_left','menu_top','menu_right','no_menu'],
      'type' : 'menu_top',
      'menuItems' : ['page1','page2','page3','page4','About','Contact']
    },
    pageItems: [
      {
        component: 'SliderComponent',
        data : {},
        children : []
      },
      {
        component: 'MapComponent',
        data : {},
        children : []
      },
      {
        component: 'FooterComponent',
        data : {},
        children : []
      },
      {
        component: 'FloatingComponent',
        data : {},
        children : []
      }

    ]
  }, null, 3));
});
router.get('/info2', function(req, res, next) {
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify({
    title: 'awesomepage',
    pageLayout : {
      // 'type' : ['menu_left','menu_top','menu_right','no_menu'],
      'type' : 'menu_left',
      'menuItems' : ['page1','page2','page3','page4','About','Contact']
    },
    pageItems: [
      {
        component: 'SliderComponent',
        data : {},
        children : []
      },
      {
        component: 'MapComponent',
        data : {},
        children : []
      },
      {
        component: 'FooterComponent',
        data : {},
        children : []
      },
      {
        component: 'FloatingComponent',
        data : {},
        children : []
      }

    ]
  }, null, 3));
});

module.exports = router;
