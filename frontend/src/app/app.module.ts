import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MenuModule} from './modules/menu/menu.module';
import {HttpClientModule} from '@angular/common/http';
import {PageComposerModule} from './modules/pagecomposer/pagecomposer.module';
import {GeneralContentModule} from './modules/generalcontent/generalcontent.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MenuModule,
    HttpClientModule,
    PageComposerModule,
    GeneralContentModule
  ],
  providers: [],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
