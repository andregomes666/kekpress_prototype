import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BaseComponent} from './components/base/base.component';
import {MenuHorizontalComponent} from './components/menuhorizontal/menuhorizontal.component';
import {MenuVerticalComponent} from './components/menuvertical/menuvertical.component';


@NgModule({
  declarations: [BaseComponent, MenuVerticalComponent, MenuHorizontalComponent],
  imports: [
    CommonModule
  ],
  exports: [MenuVerticalComponent, MenuHorizontalComponent]
})
export class MenuModule {
}
