import {Component} from '@angular/core';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-menu-vertical',
  templateUrl: 'menuvertical.component.html',
  styles: []
})
export class MenuVerticalComponent extends BaseComponent {

}
