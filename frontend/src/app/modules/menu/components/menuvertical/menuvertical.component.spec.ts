import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuverticalComponent } from './menu2.component';

describe('Menu2Component', () => {
  let component: MenuverticalComponent;
  let fixture: ComponentFixture<MenuverticalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuverticalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuverticalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
