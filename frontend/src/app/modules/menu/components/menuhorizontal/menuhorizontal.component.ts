import {Component, OnInit} from '@angular/core';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-menu-horizontal',
  templateUrl: 'menuhorizontal.component.html',
  styles: []
})
export class MenuHorizontalComponent extends BaseComponent {

}
