import { TestBed } from '@angular/core/testing';

import { PagecomposerService } from './pagecomposer.service';

describe('PagecomposerService', () => {
  let service: PagecomposerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PagecomposerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
