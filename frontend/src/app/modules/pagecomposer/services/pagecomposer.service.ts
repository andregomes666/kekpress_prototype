import {FloatingComponent} from '../../generalcontent/components/floating/floating.component';
import {Component, Injectable} from '@angular/core';
import {MenuHorizontalComponent} from '../../menu/components/menuhorizontal/menuhorizontal.component';
import {HttpClient} from '@angular/common/http';
import {MapComponent} from '../../generalcontent/components/map/map.component';
import {FooterComponent} from '../../generalcontent/components/footer/footer.component';
import {MenuVerticalComponent} from '../../menu/components/menuvertical/menuvertical.component';
import {Observable} from 'rxjs';
import {SliderComponent} from '../../generalcontent/components/slider/slider.component';

@Injectable()
export class PageComposerService {

  menuMap: Map<any, any> = new Map([
    ['MenuHorizontalComponent', MenuHorizontalComponent],
    ['MenuVerticalComponent', MenuVerticalComponent],
  ]);
  componentMap: Map<any, any> = new Map([
    ['SliderComponent', SliderComponent],
    ['MapComponent', MapComponent],
    ['FooterComponent', FooterComponent],
    ['FloatingComponent', FloatingComponent]
  ]);

  constructor(public http: HttpClient) {
  }

  getConfig(): Observable<any> {
    return this.http.get<any>('http://localhost:3000/info');
  }

  getComponent(name: string): Component {
    return this.componentMap.get(name);
  }

  getMenu(name: string): Component {
    return this.menuMap.get(name);
  }

}
