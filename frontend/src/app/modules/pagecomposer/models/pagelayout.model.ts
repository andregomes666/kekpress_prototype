export class PageLayout {
  constructor(type: string, menuItems: Array<string>) {
    this.type = type;
    this.menuItems = menuItems;
  }
  type: string;
  menuItems?: Array<string>;
}
