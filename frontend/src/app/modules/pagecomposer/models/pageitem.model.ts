export class PageItem {
  constructor(title: string, data: object, children: Array<any>) {
    this.component = title;
    this.data = data;
    this.children = children;
  }
  component: string;
  data?: object;
  children?: Array<any>;
}
