import {PageItem} from './pageitem.model';
import {PageLayout} from './pagelayout.model';

export class Page {
  constructor(title: string, pageItems: Array<PageItem>, pageLayout: PageLayout) {
    this.title = title;
    this.pageItems = pageItems;
    this.pageLayout = pageLayout;
  }

  title: string;
  pageItems?: Array<PageItem>;
  pageLayout?: PageLayout;
}
