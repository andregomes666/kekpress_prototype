import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PageComponent} from './components/page/page.component';
import {PageComposerService} from "./services/pagecomposer.service";
import {ComponentLoaderDirective} from "./directives/componentloader.directive";


@NgModule({
  declarations: [PageComponent, ComponentLoaderDirective],
  imports: [
    CommonModule,
  ],
  providers: [PageComposerService],
  exports: [PageComponent]
})
export class PageComposerModule {
}
