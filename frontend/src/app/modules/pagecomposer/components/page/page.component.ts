import {Component, OnInit} from '@angular/core';
import {PageComposerService} from '../../services/pagecomposer.service';
import {Observable} from 'rxjs';
import {Page} from '../../models/page.model';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent {
  pageData: Observable<Page>;

  constructor(public pageComposerService: PageComposerService) {
    this.pageData = this.pageComposerService.getConfig();
  }

  getComponent(name: string): any {
    return this.pageComposerService.getComponent(name);
  }
  getMenu(name: string): any {
    return this.pageComposerService.getMenu(name);
  }


}
