import {ComponentFactoryResolver, Directive, Input, OnInit, Type, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appComponentloader]'
})
export class ComponentLoaderDirective<T> implements OnInit {
  @Input() template!: Type<T>;
  @Input() data!: object;
  @Input() menuPosition = 'top';

  constructor(private viewContainerRef: ViewContainerRef, private componentFactoryResolver: ComponentFactoryResolver) {
  }

  ngOnInit(): void {
    this.loadComponent();

  }

  loadComponent(): void {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory<T>(this.template);
    const viewContainerRef = this.viewContainerRef;
    viewContainerRef.clear();
    const componentRef = viewContainerRef.createComponent<T>(componentFactory);
    // @ts-ignore
    componentRef.instance.data = this.data;
    // if (componentRef.instance.menuPosition !== undefined){
    //   componentRef.instance.menuPosition.subscribe((ev: string) => {
    //     this.menuPosition = ev;
    //     console.log(this.menuPosition);
    //   });
    // }
  }


}

