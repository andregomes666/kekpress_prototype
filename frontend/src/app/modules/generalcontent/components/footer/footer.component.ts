import { Component, OnInit } from '@angular/core';
import {BaseComponent} from "../base/base.component";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent extends BaseComponent implements OnInit {

  constructor() {
    super();
    this.componentStyle.backgroundColor = 'purple';
  }
  ngOnInit(): void {
  }

}
