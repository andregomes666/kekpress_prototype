import { Component, OnInit } from '@angular/core';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent extends BaseComponent implements OnInit {
  constructor() {
    super();
    this.componentStyle.backgroundColor = 'green';
  }

  ngOnInit(): void {
  }

}
