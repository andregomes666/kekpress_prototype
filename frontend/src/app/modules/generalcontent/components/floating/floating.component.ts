import { Component, OnInit } from '@angular/core';
import {BaseComponent} from "../base/base.component";

@Component({
  selector: 'app-floating',
  templateUrl: './floating.component.html',
  styleUrls: ['./floating.component.scss']
})
export class FloatingComponent extends BaseComponent implements OnInit {
  constructor() {
    super();
    this.componentStyle.backgroundColor = 'red';
  }

  ngOnInit(): void {
  }

}
