import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit {
  componentStyle = {
    height: '20vh',
    backgroundColor: 'blue',
    width:  '100%'
  };
  constructor() { }

  ngOnInit(): void {
  }

}
