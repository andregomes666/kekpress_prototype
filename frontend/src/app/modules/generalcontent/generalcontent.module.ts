import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseComponent } from './components/base/base.component';
import { SliderComponent } from './components/slider/slider.component';
import { MapComponent } from './components/map/map.component';
import { FloatingComponent } from './components/floating/floating.component';
import { FooterComponent } from './components/footer/footer.component';



@NgModule({
  declarations: [BaseComponent, SliderComponent, MapComponent, FloatingComponent, FooterComponent],
  imports: [
    CommonModule
  ],
  exports : [SliderComponent, MapComponent, FloatingComponent, FooterComponent]
})
export class GeneralContentModule { }
